import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Sylvain le bg
 * As Corrected by Thomas
 */

public class Monkepo extends Espece{

    private int IDunique;
    private String surnom;
    private int niveau;
    
    private int pointVie;
    private int pointExperience;
    private int pointPouvoir;
    
    private boolean estEvolution;
    private boolean estUnique;
    
    private ArrayList<Attaque> listeAttaques;
    
    private static int idgen = 0;

    public Monkepo(String surnom, boolean estEvolution, boolean estUnique, String _nom, String _couleur, double _taille, Type _type) {

    	super(_nom, _couleur, _taille, _type);
    	
    	this.IDunique = idgen;
    	
        this.surnom = surnom;
        this.niveau = new Random().nextInt((100 - 1) + 1) + 1 ;
        
        this.pointVie = 100;
        this.pointExperience = 0;
        this.pointPouvoir = 10;
        
        this.estEvolution = estEvolution;
        this.estUnique = estUnique;
        
        this.listeAttaques = new ArrayList<>();
        
        if(estUnique) {
        	this.niveau = 100;
        	this.estEvolution = true;
        	this.pointPouvoir = 100;
        }
        
        this.idgen++;
    };
    
    @Override
    public void evoluer() {
    	this.niveau = 100;
    	this.estEvolution = true;
    	this.pointPouvoir = 100;
    }


    /**
     * @return int return the IDunique
     */
    public int getIDunique() {
        return IDunique;
    }

    /**
     * @return String return the surnom
     */
    public String getSurnom() {
        return surnom;
    }

    /**
     * @return int return the niveau
     */
    public int getNiveau() {
        return niveau;
    }

    /**
     * @param niveau the niveau to set
     */
    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    /**
     * @return int return the pointVie
     */
    public int getPointVie() {
        return pointVie;
    }

    /**
     * @param pointVie the pointVie to set
     */
    public void setPointVie(int pointVie) {
        this.pointVie = pointVie;
    }

    /**
     * @return int return the pointExperience
     */
    public int getPointExperience() {
        return pointExperience;
    }

    /**
     * @param pointExperience the pointExperience to set
     */
    public void setPointExperience(int pointExperience) {
        this.pointExperience = pointExperience;
    }

    /**
     * @return int return the pointPouvoir
     */
    public int getPointPouvoir() {
        return pointPouvoir;
    }

    /**
     * @param pointPouvoir the pointPouvoir to set
     */
    public void setPointPouvoir(int pointPouvoir) {
        this.pointPouvoir = pointPouvoir;
    }

    public boolean estKo() {
        if (pointVie == 0) {
            return true;
        } else
            return false;
    }

    public void calculerNiveau() {
    	this.niveau = this.pointExperience / 100;
    }

    public boolean peutEvoluer() {
        if (this.getNiveau() == 100) {
            if (this.estEvolution == true) {
                return false;
            } else {
            	return true;
            }
        }
        return false;
    }
    
    // Retourne une probabilite de capture pour un Monkepo (entre 0 et 1)
    public boolean calculerProbaCapture() {
    	Random random = new Random();
    	
    	double proba = 1.0/this.getNiveau();
    	
    	return random.nextDouble() <= proba;
    }
    
    public Attaque choisirAttaque(int nbAttaque) {
    	if(this.listeAttaques.get(nbAttaque).estLancable(this)) {
    		return this.listeAttaques.get(nbAttaque);
    	} else {
    		return null;
    	}
    }
    
    public ArrayList<Attaque> getListeAttaque(){
    	return this.listeAttaques;
    }
    
    public void setListeAttaque(ArrayList<Attaque> liste) {
    	this.listeAttaques =  liste;
    }
    
    public void encaisserDegats(int degats) {
    	this.pointVie -= degats;
    	
    	if(this.pointVie < 0) {
    		this.pointVie = 0;
    	}
    }
    
    public void majPointsPouvoir() {
    	this.pointPouvoir -= 1;
    }
    
    public void afficherAttaquesDispos() {
    	for(Attaque a : this.listeAttaques) {
    		System.out.println("\t - Attaque : " + a.getNom() + ", puissance : " + a.getValeurPuissance() + ", cout Points de Vies : " + a.getCoutPP());
    	}
    }

}
