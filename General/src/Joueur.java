import java.util.Arrays;

public class Joueur extends Dresseur{

	private String[] badgesArenes;
	private int nbMonkeball;
	
	private int positionX;
	private int positionY;
	
	public Joueur(String _nom, String _prenom, double _sommeMonkedollar) {
		super(_nom, _prenom, _sommeMonkedollar);
		this.badgesArenes = new String[3];
		this.nbMonkeball = 0;
		
		this.positionX = 9;
		this.positionY = 11;
	}
	
	
	public int getPositionX() {
		return this.positionX;
	}
	
	public int getPositionY() {
		return this.positionY;
	}
	

	public int getNbMonkeball() {
		return nbMonkeball;
	}


	public void setNbMonkeball(int nbMonkeball) {
		this.nbMonkeball = nbMonkeball;
	}



	public String[] getBadgesArenes() {
		return badgesArenes;
	}
	
	public void afficherBadges() {
		String result = "Liste des Badges = [";
		
		for(String s : this.badgesArenes) {
			result += s + ", ";
		}
		
		result += "]";
		
		System.out.println(result);
	}
	
	public void ajouterUnBadge(String badge) {
		for(int i = 0; i < this.badgesArenes.length; i++) {
			if(this.badgesArenes[i] == null) {
				this.badgesArenes[i] = badge;
				break;
			}
		}
	}
	

	// methode pour tenter de capture un Monkepo
	public void capturer(Monkepo m) {
		
		if(this.nbMonkeball != 0) {
			
			if(m.calculerProbaCapture()) {
				
				this.ajouterABanque(m);
				System.out.println("Tu as capture le Monkepo bravo !!!");
				this.nbMonkeball -= 1;
				
			} else {
				
				System.out.println("Tu n'as pas reussi ta capture, tu perds ta monkeball");
				this.nbMonkeball -= 1;
				
			}
			
		} else {
			System.out.println("Tu n'as pas de Monkeball, tu ne peux pas capturer");
		}
		
	}
	
	// envoie un Monkepo de la banque vers le sac A Dos
	public void envoyerMonkepoBanque(Monkepo m) {
		for(Monkepo dansLaBanque : this.getBanque()) {
			if(dansLaBanque == m) {
				
				// si le sac n'est pas plein
				if(this.sacIsFull() == false) {
						
					this.placerDansLeSac(dansLaBanque);
					this.retirerBanque(dansLaBanque);
					System.out.println("Le Monkepo a ete vire dans ton sac");

					
				} else {
					System.out.println("Sac a dos plein, operation impossible");
				}
				break;
			}
			
		}
	}
	
	// envoie un Monkepo du sac vers la banque
	public void envoyerMonkepoSac(Monkepo m) {
		for(Monkepo dansLeSac : this.getSacADos()) {
			if(dansLeSac == m) {
				this.ajouterABanque(m);
				this.retirerSacADos(m);
				System.out.println("Le Monkepo a ete vire vers ta banque");
				
				break;
			}
		}
	}
	
	public void modifierRangMonkepo(int rangActuel, int rangVoulu) {
		if(rangVoulu > 5 || rangVoulu < 0 || rangActuel < 0 || rangActuel > 5) {
			System.out.println("Depassement de la taille du tableau, tu ne peux pas faire ca woh !!");
		}
		
		Monkepo[] sacTemp = this.getSacADos();
		
		Monkepo monkepoDuRangDepart = sacTemp[rangVoulu];
		
		sacTemp[rangVoulu] = sacTemp[rangActuel];
		sacTemp[rangActuel] = monkepoDuRangDepart;
		
		this.setSacADos(sacTemp);
	}
	
	
	// permet d'avancer d'une case 
	// vers l'avant
	// vers l'arriere
	// sur la gauche
	// sur la droite
	public void avancer(Carte c) {
		// on récupère le tablo en int
		int[][] temp = c.getTableauInt();

		
		// maintenant qu'on a la position on regarde si l'avancer est possible 
		if(this.positionX < 22){
			if(temp[this.positionX + 1][this.positionY] != 4 || temp[this.positionX + 1][this.positionY] != 5) {
				// remettre la case par défaut
				temp = c.getTableauParDefaut();

				// faire avancer le joueur
				temp[this.positionX+1][this.positionY ] = 666;
				this.positionX += 1;
			}
		}

		// On set le nouveau tableau :
		c.setTabloenint(temp);
	}
	
	public void reculer(Carte c) {
		// on récupère le tablo en int
		int[][] temp = c.getTableauInt();

		
		// maintenant qu'on a la position on regarde si l'avancer est possible 
		if(this.positionX >= 0){
			if(temp[this.positionX - 1][this.positionY] != 4 || temp[this.positionX - 1][this.positionY] != 5) {
				// remettre la case par défaut
				temp = c.getTableauParDefaut();

				// faire avancer le joueur
				temp[this.positionX-1][this.positionY ] = 666;
				this.positionX -= 1;
			}
		}

		// On set le nouveau tableau :
		c.setTabloenint(temp);
	}
	
	public void monter(Carte c) {
		// on récupère le tablo en int
		int[][] temp = c.getTableauInt();

		
		// maintenant qu'on a la position on regarde si l'avancer est possible 
		if(this.positionY < 22){
			if(temp[this.positionX][this.positionY + 1] != 4 || temp[this.positionX][this.positionY + 1] != 5) {
				// remettre la case par défaut
				temp = c.getTableauParDefaut();

				// faire avancer le joueur
				temp[this.positionX][this.positionY + 1] = 666;
				this.positionY += 1;
			}
		}

		// On set le nouveau tableau :
		c.setTabloenint(temp);
	}
	
	public void descendre(Carte c) {
		// on récupère le tablo en int
		int[][] temp = c.getTableauInt();

		
		// maintenant qu'on a la position on regarde si l'avancer est possible 
		if(this.positionY >= 0){
			if(temp[this.positionX][this.positionY - 1] != 4 || temp[this.positionX][this.positionY - 1] != 5) {
				// remettre la case par défaut
				temp = c.getTableauParDefaut();

				// faire avancer le joueur
				temp[this.positionX][this.positionY - 1] = 666;
				this.positionY -= 1;
			}
		}

		// On set le nouveau tableau :
		c.setTabloenint(temp);
	}
	

	// Ajoute une Monkeball au joueur
	public void ajouterMonkeball(int nbMonkeball) {
		this.nbMonkeball += nbMonkeball;
	}

}
