import java.time.LocalDate;
import java.util.Random;

public class Route extends Case{
	
	private NonJoueur pnj;

	public Route(int _x, int _y, boolean _bloque, boolean _rapide) {
		super(_x, _y, _bloque, _rapide);
		this.pnj = null;
	}
	
	public boolean calculerProbaCroiserDresseur() {
		Random rand = new Random();

    	double proba = 1.0/ 4;
    	
    	return rand.nextDouble() <= proba;
	}
	
	public void placerDresseurSurCase(NonJoueur d) {
		if(this.calculerProbaCroiserDresseur() && this.pnj == null) {
			this.pnj = d;
		}
	}
	
	// En theorie
	// On creer un combat
	// On prend la date actuel
	// on laisse la classe Combat gerer la chose
	public void initialiserCombat(Joueur j) {
		if(this.pnj != null){
			Combat c = new Combat(j, this.pnj, null, LocalDate.now());
			
			boolean faireCombat = c.combattreDresseur();
			
			// Si on gagne
			if(faireCombat) {
				this.pnj = null;
			}
		}
	}

}
