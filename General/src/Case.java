
public abstract class Case {
	private int positionX;
	private int positionY;
	protected boolean bloque;
	private boolean rapide;
	
	public Case(int _x, int _y, boolean _bloque, boolean _rapide) {
		this.positionX = _x;
		this.positionY = _y;
		this.bloque = _bloque;
		this.rapide = _rapide;
	}
	
	public boolean estBloque() {
		return this.bloque;
	}
	
	public boolean estRapide() {
		return this.rapide;
	}
	
	public void setBloque(boolean _obstacle) {
		this.bloque = _obstacle;
	}
	
}
