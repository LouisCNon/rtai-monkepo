import java.util.ArrayList;

public class Clan {
	private ArrayList<Dresseur> membres;
	private double sommeTotal;
	private NomClan nom;
	
	public Clan(NomClan _nom, ArrayList<Dresseur> _membres) {
		this.nom = _nom;
		this.membres = _membres;
		
		this.sommeTotal = 0;
		
		for(Dresseur d : this.membres) {
			this.sommeTotal += d.getSommeMonkedollar();
		}
	}

	public ArrayList<Dresseur> getMembres() {
		return membres;
	}

	public void setMembres(ArrayList<Dresseur> membres) {
		this.membres = membres;
	}

	public double getSommeTotal() {
		return sommeTotal;
	}

	public void setSommeTotal(double sommeTotal) {
		this.sommeTotal = sommeTotal;
	}

	public NomClan getNom() {
		return nom;
	}

	public void setNom(NomClan nom) {
		this.nom = nom;
	}
	
	public void ajouterMembre(Dresseur d) {
		this.membres.add(d);
	}
	
	public void recalculerSommeTotal() {
		this.sommeTotal = 0;
		
		for(Dresseur d : this.membres) {
			this.sommeTotal += d.getSommeMonkedollar();
		}		
	}
}
