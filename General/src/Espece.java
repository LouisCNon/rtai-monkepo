/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sylvai
 */
public abstract class Espece {
    
    private String nom;
    private String couleur;
    private double taille;
    private Type typeEspece;
    
    public Espece(String _nom ,String _couleur, double _taille, Type _type){
	    this.nom = _nom;
	    this.couleur = _couleur;
	    this.taille= _taille;
	    this.typeEspece = _type;
    
    }
    
    public Type getType() {
    	return this.typeEspece;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public double getTaille() {
        return taille;
    }

    public void setTaille(double taille) {
        this.taille = taille;
    }
    
    public void evoluer(){
        
    }
    
    public int getProba() {
    	int result = 1; 
    	
    	if(this.typeEspece == Type.FEU) {
    		result = 4;
    	}
    	if(this.typeEspece == Type.EAU) {
    		result = 8;
    	}
    	if(this.typeEspece == Type.PLANTE) {
    		result = 6;
    	}
    	
    	return result;
    }
}
