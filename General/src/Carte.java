import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Carte {
	private static final int ROWS = 23;
	private static final int COLUMNS = 23;
	private int[][] tabloenint;
	private int[][] tabloParDefaut;
	
	private Joueur joueur;
	
	private JFrame kanto;
	
	public Carte(Joueur j) throws Exception{
		this.joueur = j;
		
		  BufferedReader br = new BufferedReader(new FileReader("png/carteKanto.csv"));
		  List<String[]> ligne = new ArrayList<String[]>();
		  String thisLine = null;
	    
		  while ((thisLine = br.readLine()) != null){
			  
			 ligne.add(thisLine.split("\n"));
			 
		  }
	    
		  String[][] carte = new String[ligne.size()][0];
		  
		  ligne.toArray(carte);
		  
		  
		  // Transformer la map en un tableau de int
		  this.tabloenint = new int[ROWS][COLUMNS];
		  int[] test = new int[23];
		  
		  for(int i = 0; i < 23; i++) {
			  String[] toCharArray = carte[i][0].split(",");
			  
			  test = fromCharToInt(toCharArray);
			  this.tabloenint[i] = test;
		  }
		  
		  this.tabloParDefaut = this.tabloenint;
		  
		  
		  br.close();
		 
	}

	// Selectionne pour chaque case du tableau d'entier la bonne image et créer une liste d'objet associé
	public void creerInstanceCarte() throws Exception{
		
		  this.kanto = new JFrame("Monde de Kanto");
		  this.kanto.setVisible(true);
		
		  GridLayout myLayout = new GridLayout(23, 23);
		  
		  this.kanto.setLayout(myLayout);
		  
		  for(int i = 0; i < ROWS; i++) {
			  for(int j = 0; j < COLUMNS; j++) {
				  BufferedImage image;
				  
				  // if(Integer.parseInt(carte[i][j]) == 1) {
					  image = ImageIO.read(new File("png/1.png"));
				 //  }
				  if(this.tabloenint[i][j] == 2) {
					  image = ImageIO.read(new File("png/2.png"));
				  }
				  if(this.tabloenint[i][j] == 3) {
					  image = ImageIO.read(new File("png/3.png"));
				  }
				  if(this.tabloenint[i][j] == 4) {
					  image = ImageIO.read(new File("png/4.png"));
				  }
				  if(this.tabloenint[i][j] == 5) {
					  image = ImageIO.read(new File("png/5.png"));
				  }
				  if(this.tabloenint[i][j] == 61) {
					  image = ImageIO.read(new File("png/arene/plante/61.png"));
				  }
				  if(this.tabloenint[i][j] == 62) {
					  image = ImageIO.read(new File("png/arene/plante/62.png"));
				  }
				  if(this.tabloenint[i][j] == 63) {
					  image = ImageIO.read(new File("png/arene/plante/63.png"));
				  }
				  if(this.tabloenint[i][j] == 64) {
					  image = ImageIO.read(new File("png/arene/plante/64.png"));
				  }
				  if(this.tabloenint[i][j] == 65) {
					  image = ImageIO.read(new File("png/arene/plante/65.png"));
				  }
				  if(this.tabloenint[i][j] == 66) {
					  image = ImageIO.read(new File("png/arene/plante/66.png"));
				  }
				  if(this.tabloenint[i][j] == 67) {
					  image = ImageIO.read(new File("png/arene/plante/67.png"));
				  }
				  if(this.tabloenint[i][j] == 68) {
					  image = ImageIO.read(new File("png/arene/plante/68.png"));
				  }
				  if(this.tabloenint[i][j] == 69) {
					  image = ImageIO.read(new File("png/arene/plante/69.png"));
				  }
				  if(this.tabloenint[i][j] == 71) {
					  image = ImageIO.read(new File("png/arene/eau/71.png"));
				  }
				  if(this.tabloenint[i][j] == 72) {
					  image = ImageIO.read(new File("png/arene/eau/72.png"));
				  }
				  if(this.tabloenint[i][j] == 73) {
					  image = ImageIO.read(new File("png/arene/eau/73.png"));
				  }
				  if(this.tabloenint[i][j] == 74) {
					  image = ImageIO.read(new File("png/arene/eau/74.png"));
				  }
				  if(this.tabloenint[i][j] == 75) {
					  image = ImageIO.read(new File("png/arene/eau/75.png"));
				  }
				  if(this.tabloenint[i][j] == 76) {
					  image = ImageIO.read(new File("png/arene/eau/76.png"));
				  }
				  if(this.tabloenint[i][j] == 77) {
					  image = ImageIO.read(new File("png/arene/eau/77.png"));
				  }
				  if(this.tabloenint[i][j] == 78) {
					  image = ImageIO.read(new File("png/arene/eau/78.png"));
				  }
				  if(this.tabloenint[i][j] == 79) {
					  image = ImageIO.read(new File("png/arene/eau/79.png"));
				  }
				  if(this.tabloenint[i][j] == 81) {
					  image = ImageIO.read(new File("png/arene/feu/81.png"));
				  }
				  if(this.tabloenint[i][j] == 82) {
					  image = ImageIO.read(new File("png/arene/feu/82.png"));
				  }
				  if(this.tabloenint[i][j] == 83) {
					  image = ImageIO.read(new File("png/arene/feu/83.png"));
				  }
				  if(this.tabloenint[i][j] == 84) {
					  image = ImageIO.read(new File("png/arene/feu/84.png"));
				  }
				  if(this.tabloenint[i][j] == 85) {
					  image = ImageIO.read(new File("png/arene/feu/85.png"));
				  }
				  if(this.tabloenint[i][j] == 86) {
					  image = ImageIO.read(new File("png/arene/feu/86.png"));
				  }
				  if(this.tabloenint[i][j] == 87) {
					  image = ImageIO.read(new File("png/arene/feu/87.png"));
				  }
				  if(this.tabloenint[i][j] == 88) {
					  image = ImageIO.read(new File("png/arene/feu/88.png"));
				  }
				  if(this.tabloenint[i][j] == 89) {
					  image = ImageIO.read(new File("png/arene/feu/89.png"));
				  }
				  if(this.tabloenint[i][j] == 9) {
					  image = ImageIO.read(new File("png/9.png"));
				  }
				  if(this.tabloenint[i][j] == 10) {
					  image = ImageIO.read(new File("png/10.png"));
				  }
				  
				  // L'image du joueur
				  if(this.tabloenint[i][j] == 666) {
					  image = ImageIO.read(new File("png/joueur/joueur.png"));
				  }
				  
				  
				  JLabel uneCase  = new JLabel(new ImageIcon(image));
				  uneCase.addKeyListener(new MyKeyListener(this, this.joueur));
				  this.kanto.add(uneCase);
			  }
		  }
		  this.kanto.setSize(ROWS * 25, COLUMNS * 25);
	}
	

	
	public int[][] getTableauInt(){
		return this.tabloenint;
	}
	
	public void setTabloenint(int[][] _tablo) {
		this.tabloenint = _tablo;
	}
	
	public int[][] getTableauParDefaut(){
		return this.tabloParDefaut;
	}
	
	public static int[] fromCharToInt(String[] characterTab) {
		int[] tablo = new int[23];
		
		for(int i = 0; i < 23; i++) {
			tablo[i] = Integer.parseInt(characterTab[i]);
		}
		
		return tablo;
	}

}


class MyKeyListener extends KeyAdapter {
	  private Joueur j;
	  private Carte c;
	  
	  public MyKeyListener(Carte _c, Joueur _j) throws Exception{
		  this.j = _j;
		  this.c = _c;
	  }
	  
	  public void keyPressed(KeyEvent evt)  {
		    if (evt.getKeyChar() == 'z'){
		    	this.j.monter(this.c);
		    	System.out.println("Monter");
		    	try {
					this.c.creerInstanceCarte();
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		    if(evt.getKeyChar() == 'q') {
		    	this.j.reculer(this.c);
		    }
		    if(evt.getKeyChar() == 's') {
		    	this.j.descendre(this.c);
		    }
		    if(evt.getKeyChar() == 'd') {
		    	this.j.avancer(this.c);
		    }
	  }
}