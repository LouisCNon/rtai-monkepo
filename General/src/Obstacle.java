public class Obstacle extends Case{

	private String type;
	
	public Obstacle(int _x, int _y, boolean _bloque, boolean _rapide, String _type) {
		super(_x, _y, _bloque, _rapide);
		this.type = _type;
	}
	
	@Override
	public void setBloque(boolean _obstacle) {
		this.bloque = true;
	}
	
	
	// si c'est un obstacle alors c'est un caillou (true)
	//Sinon c'est de l'eau (false)
	public boolean estObstacle() {
		if(this.type.compareTo("Eau") == 0) {
			return false;
		}
		return true;
	}
	
}
