import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class Combat {
	private Joueur joueur;
	private NonJoueur nonJoueur;
	private Monkepo monkepo;
	private LocalDate dateCombat;
	private String resultat;
	
	public Combat(Joueur _joueur, NonJoueur _nonjoueur, Monkepo _monkepo, LocalDate _date) {
		this.joueur = _joueur;
		
		if(_nonjoueur == null) {
			this.monkepo = _monkepo;
			this.nonJoueur = null;
		} else {
			this.nonJoueur = _nonjoueur;
			this.monkepo = null;
		} 
		
		this.dateCombat = _date;
		this.resultat = "";
	}
	
	// retourne true si on gagne
	// false sinon
	public boolean combattreMonkepo() {
		
		System.out.println("Tu viens de croiser un Monkepo, tu vas te battre");
		
		// on cherche le premier monkepo du sac
		Monkepo combattant = new Monkepo("", false, false, "", "", 0.0, null);
		int rangMonkepoDepart = 0;
		
		for(int i = 0; i < this.joueur.getSacADos().length; i++) {
			if(this.joueur.getSacADos()[i] !=null && this.joueur.getSacADos()[i].getPointVie() > 0) {
				combattant = this.joueur.getSacADos()[i];
				rangMonkepoDepart = i;
				break;
			}
		}
		
		while(this.monkepo.getPointVie() > 0 && this.joueur.possedeEncoreMonkepo()) {
			System.out.println("Joueur, c'est a toi de jouer");
			System.out.println("Choisis une attaque");
			System.out.println("- 1 : Attaquer");
			System.out.println("- 2 : Capturer");
			System.out.println("- 3 : Echanger");
			
			Scanner choix = new Scanner(System.in);
			
			
			if(choix.nextInt() == 1) {

				System.out.println("Choisir une attaque :");
				// Affichage des attaques disponibles
				combattant.afficherAttaquesDispos();
				
				Scanner choixUser = new Scanner(System.in);
				
				combattant.majPointsPouvoir();
				
				// envoyer degats sur le monkepo adverse
				Efficacite effAttaque = new Efficacite(this.monkepo, combattant.choisirAttaque(choixUser.nextInt()));
				
				
				System.out.println("Tu as envoye ton attaque et inflige : " + effAttaque.calculerEfficacite());
				this.monkepo.encaisserDegats(effAttaque.calculerEfficacite());
				
			}
			if(choix.nextInt() == 2) {
				
				System.out.println("Tentative de capture du Monkepo");
				this.joueur.capturer(this.monkepo);
				
			}
			if(choix.nextInt() == 3) {
				
				System.out.println("Voici ton sac");
				this.joueur.afficherSac();
				System.out.println("Choisi le rang du Monkepo avec lequel tu veux echanger");
				Scanner echange = new Scanner(System.in);
				
				this.joueur.modifierRangMonkepo(rangMonkepoDepart, echange.nextInt());
				
			}			
			
			System.out.println("C'est au tour du Monkepo");
			
			// on selectionne une attaque random pour un Monkepo
			int choixAttaque = new Random().nextInt(3);
			
			
			this.monkepo.choisirAttaque(choixAttaque);
			this.monkepo.majPointsPouvoir();
			
			System.out.println("Le Monkepo t'attaque avec " + this.monkepo.getListeAttaque().get(choixAttaque).getNom());
			
			Efficacite encaisser = new Efficacite(combattant, this.monkepo.getListeAttaque().get(choixAttaque));
			
			combattant.encaisserDegats(encaisser.calculerEfficacite());
			
			System.out.println("Tu viens de prendre : " + encaisser.calculerEfficacite() + " degats");
			
			
			// si notre monkepo meurt de son combat
			if(combattant.getPointVie() <= 0) {
				
				System.out.println("Ton Monkepo est mort, le suivant prend les commandes");
				
				// on passe le Monkepo du joueur en mort
				this.joueur.getSacADos()[rangMonkepoDepart].setPointVie(0);
				
				// On prend le Monkepo qui vient apres
				for(int j = rangMonkepoDepart; j < this.joueur.getSacADos().length; j++) {
					
					if(this.joueur.getSacADos()[j] != null && this.joueur.getSacADos()[j].getPointVie() > 0) {
						combattant = this.joueur.getSacADos()[j];
						rangMonkepoDepart = j;
						break;
					}
				}
			}
			
		}
		
		// On repartie les XP du Monkepo pour nous si on gagne
		if(this.monkepo.getPointVie() <= 0) {
			// on repartie pour chaque monkepo du sac les XP
			for(Monkepo m : this.joueur.getSacADos()) {
				m.setPointExperience(m.getPointExperience() + this.monkepo.getPointExperience());
				m.calculerNiveau();
				
				if(m.getNiveau() == 100 && m.peutEvoluer()) {
					m.evoluer();
				}
			}
			
			// on inscrit le resultat
			this.resultat = "Le joueur gagne le combat";
			return true;
		} else {
			this.resultat = "Le Monkepo gagne le combat";
			return false;
		}

	}
	
	
	
	
	
	// retourne true si on gagne
	// false sinon
	public boolean combattreDresseur() {
		
		// Si le dresseur est militant
		if(this.nonJoueur.getEstMilitant()) {
			
			// on prend le Monkepo de rang 1 et on le supprime des Monkepos du dresseur
			System.out.println("Tu as croisé un Militant, il te vole ton Monkepo.");
			this.nonJoueur.volerMonkepo(this.joueur);
			return false;
			
		} else {
			
			this.joueur.sePresenter();
			this.nonJoueur.sePresenter();
			
			// selectionner Monkepo rang le plus faible du Joueur
			Monkepo combattant = new Monkepo("", false, false, "", "", 0.0, null);;
			int rangMonkepoDepart = 0;
			
			for(int i = 0; i < this.joueur.getSacADos().length; i++) {
				if(this.joueur.getSacADos()[i] !=null && this.joueur.getSacADos()[i].getPointVie() > 0) {
					combattant = this.joueur.getSacADos()[i];
					rangMonkepoDepart = i;
					break;
				}
			}
			
			// Selectionner Monkepo rang le plus faible du nonJoueur
			Monkepo adversaire = new Monkepo("", false, false, "", "", 0.0, null);;
			int rangMonkepoAdversaire = 0;
			
			for(int i = 0; i < this.nonJoueur.getSacADos().length; i++) {
				if(this.nonJoueur.getSacADos()[i] !=null && this.nonJoueur.getSacADos()[i].getPointVie() > 0) {
					adversaire = this.nonJoueur.getSacADos()[i];
					rangMonkepoAdversaire = i;
					break;
				}
			}
			
			while(this.nonJoueur.possedeEncoreMonkepo() && this.joueur.possedeEncoreMonkepo()) {
				
				System.out.println(this.joueur.getNom() + ", c'est a toi de jouer");
				System.out.println("Choisis une attaque");
				System.out.println("- 1 : Attaquer");
				System.out.println("- 2 : Echanger");
				Scanner choix = new Scanner(System.in);
				
				if(choix.nextInt() == 1) {
					
					System.out.println("Choisis un nombre d'attaque entre 0 et 3");
					combattant.afficherAttaquesDispos();
					
					Scanner choixUser = new Scanner(System.in);
					
					Efficacite encaisser = new Efficacite(adversaire, combattant.choisirAttaque(choixUser.nextInt()));
					
					System.out.println("Tu as choisi d'attaquer avec " + combattant.getListeAttaque().get(choixUser.nextInt()).getNom() + " et tu infliges " + combattant.getListeAttaque().get(choixUser.nextInt()).getValeurPuissance());
					
					adversaire.encaisserDegats(encaisser.calculerEfficacite());
					
					// si son monkepo meurt de notre attaque
					if(adversaire.getPointVie() <= 0) {
						
						System.out.println("Tu as tué son Monkepo !!");
						
						// on passe le Monkepo du joueur en mort
						this.nonJoueur.getSacADos()[rangMonkepoAdversaire].setPointVie(0);
						
						// On prend le Monkepo qui vient apres
						for(int j = rangMonkepoAdversaire; j < this.nonJoueur.getSacADos().length; j++) {
							
							if(this.nonJoueur.getSacADos()[j] != null && this.nonJoueur.getSacADos()[j].getPointVie() > 0) {
								adversaire = this.nonJoueur.getSacADos()[j];
								rangMonkepoAdversaire = j;
								break;
							}
							
						}
					}
					
				}
				if(choix.nextInt() == 2) {
					
					System.out.println("Voici ton sac : ");
					this.joueur.afficherSac();
					
					System.out.println("Choisi le rang du Monkepo avec lequel tu veux echanger");
					Scanner echange = new Scanner(System.in);
					
					this.joueur.modifierRangMonkepo(rangMonkepoDepart, echange.nextInt());		
					
					System.out.println("Voici ta nouvelle configuration : ");
					this.joueur.afficherSac();
				}

				// on selectionne une attaque random pour un nonJoueur
				int choixAttaque = new Random().nextInt(4);			
				
				Efficacite encaisser = new Efficacite(combattant, adversaire.getListeAttaque().get(choixAttaque));
				
				System.out.println("");
				System.out.println("C'est au tour de " + this.nonJoueur.getNom() + " " + this.nonJoueur.getPrenom() + " de jouer !");
				System.out.println("Son Monkepo t'attaque avec " + adversaire.getListeAttaque().get(choixAttaque).getNom() + " et t'inflige " + adversaire.getListeAttaque().get(choixAttaque).getValeurPuissance());
				
				combattant.encaisserDegats(encaisser.calculerEfficacite());
				
				
				// si notre monkepo meurt de son combat
				if(combattant.getPointVie() <= 0) {
					
					// on passe le Monkepo du joueur en mort
					this.joueur.getSacADos()[rangMonkepoDepart].setPointVie(0);
					
					// On prend le Monkepo qui vient apres
					for(int j = rangMonkepoDepart; j < this.joueur.getSacADos().length; j++) {
						
						if(this.joueur.getSacADos()[j] != null && this.joueur.getSacADos()[j].getPointVie() > 0) {
							combattant = this.joueur.getSacADos()[j];
							rangMonkepoDepart = j;
							break;
						}
						
					}
				}
			}
			
			// si on a gagne 
			if(this.nonJoueur.possedeEncoreMonkepo() == false) {
				
				for(Monkepo m : this.joueur.getSacADos()) {
					m.setPointExperience(m.getPointExperience() + adversaire.getPointExperience());
					m.calculerNiveau();
					
					if(m.getNiveau() == 100 && m.peutEvoluer()) {
						m.evoluer();
					}
				}
				
				// on gagne 10 de son argent
				this.nonJoueur.verserSomme(this.joueur);
				return true;
				
			} // si on perd 
			else {
				
				this.joueur.verserSomme(this.nonJoueur);
				return false;
				
			}
			
		}

	}
	
	
}
