import java.util.Random;

import java.time.*;

public class HautesHerbes extends Case{
	
	private Monkepo monkepo;

	public HautesHerbes(int _x, int _y, boolean _bloque, boolean _rapide) {
		super(_x, _y, _bloque, _rapide);
		this.monkepo = null;
	}
	
	public boolean calculerProbaCroiserMonkepo(Monkepo m) {
		Random rand = new Random();

    	double proba = 1.0/ m.getProba();
    	
    	return rand.nextDouble() <= proba;
	}
	
	public void placerMonkepoSurCase(Monkepo m) {
		if(this.calculerProbaCroiserMonkepo(m) && this.monkepo == null) {
			this.monkepo = m;
		}
	}
	
	// En theorie
	// On creer un combat
	// On prend la date actuelle
	// on laisse la classe Combat gerer la chose
	public void initialiserCombat(Joueur j) {
		if(this.monkepo != null){
			Combat c = new Combat(j, null, this.monkepo, LocalDate.now());
			
			boolean faireCombat = c.combattreMonkepo();
			
			if(faireCombat) {
				this.monkepo = null;
			}
		} else {
			System.out.println("Il n'y a pas de Monkepo ici");
		}
	}

}
