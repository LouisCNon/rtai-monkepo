import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws InterruptedException, Exception {
		Joueur joueur = new Joueur("Lamothe", "Thomas", 20);
		
		NonJoueur pnj1 = new NonJoueur("Tatler", "James", 200, false, 10, 4);
        NonJoueur pnj2 = new NonJoueur("Durand","Philippe",150,false,15,5);
        NonJoueur pnj3 = new NonJoueur("Oui","Oui",500,false,20,14);
        NonJoueur pnj4 = new NonJoueur("B2O","BA",1000,false,4,15);
        NonJoueur pnj5 = new NonJoueur("Roumanoff","Anne",300,false,7,8);
        NonJoueur pnj6 = new NonJoueur("Plancade","Robin",400,false,5,5);
        NonJoueur pnj7 = new NonJoueur("Gonnet","Ulysse",400,false,6,6);
        NonJoueur pnj8 = new NonJoueur("Pons","Sylvain",400,false,8,8);
        NonJoueur pnj9 = new NonJoueur("Lamothe","Thomas",400,false,2,2);
        NonJoueur pnj10 = new NonJoueur("Collaine","Louis",50,false,12,12);
        NonJoueur pnj11 = new NonJoueur("Michalak","David",200,false,10,10);
        NonJoueur pnj12 = new NonJoueur("Amstrong","Lans",250,false,11,11);
        NonJoueur pnj13 = new NonJoueur("Balkany","Patrick",1000,false,15,15);
        NonJoueur pnj14 = new NonJoueur("Fillon","Francois",1500,false,19,19);
        NonJoueur pnj15 = new NonJoueur("Jr","Robert",320,false,18,18);
        NonJoueur pnj16 = new NonJoueur("Johnson","Dwayne",1500,false,13,15);
        NonJoueur pnj17 = new NonJoueur("Le maréchal","Pétain",500,false,14,18);
        NonJoueur pnj18 = new NonJoueur("Camus","Albert",200,false,20,1);
        NonJoueur pnj19 = new NonJoueur("Drucker","Michel",360,false,1,8);
        NonJoueur pnj20 = new NonJoueur("Papa","Johnny",0,false,8,16);
        NonJoueur pnj21 = new NonJoueur("Jean","Neymar",630,false,5,19);
        NonJoueur pnj22 = new NonJoueur("La","Police",300,false,20,20);
        NonJoueur pnj23 = new NonJoueur("Fugain","Michel",140,false,19,5);
        NonJoueur pnj24 = new NonJoueur("Griezmann","Antoine",1000,false,14,19);
		NonJoueur pnjMilitant = new NonJoueur("Lupin", "Arsene", 2000, true, 10, 4);
		
		Monkepo pikachu = new Monkepo("Pikachu", false, false, "Nom", "bleu", 52, Type.FEU);
		Monkepo dracofeu = new Monkepo("Dracofeu", false, true, "Nom", "bleu", 32, Type.FEU);
		Monkepo salameche = new Monkepo("Salameche", false, false, "Nom", "bleu", 10, Type.FEU);
		Monkepo bulbizare = new Monkepo("Bulbizarre", false, false, "Nom", "bleu", 21, Type.FEU);
        Monkepo stud = new Monkepo("Stud", true, true,"Flemmard","Noir",1.0,Type.EAU);
        Monkepo staross = new Monkepo("Staross", false, true,"Etoile","Violet",0.50,Type.EAU);
        Monkepo prof = new Monkepo("Prof", true, true,"Humain","Blanc",1.20,Type.FEU);
        Monkepo exam = new Monkepo("Exam",true, true,"Papier","Jaune",0.20,Type.PLANTE);
        Monkepo caninos = new Monkepo("Caninos",false,false,"Chien","Orange",0.15,Type.FEU);
        Monkepo insecateur = new Monkepo("Insecateur",false,true,"Mante","Vert",1,Type.PLANTE);
        Monkepo chetiflor = new Monkepo("Chetiflor",false,false,"Ventouse","Vert/Jaune",0.20,Type.PLANTE);
        Monkepo feunard = new Monkepo("Feunard",false,true,"Chien","Jaune",1.20,Type.FEU);
        Monkepo carapuce = new Monkepo("Carapuce",false,false,"Tortue","Bleu",0.50,Type.EAU);
        Monkepo minidraco = new Monkepo("Minidraco",false,false,"Dragon","Bleu",0.30,Type.EAU);
        Monkepo magicarpe = new Monkepo("Magicarpe",false,false,"Carpe","Rouge",0.10,Type.EAU);
        Monkepo Aquali = new Monkepo("Aquali",false,true,"Chien","Bleu",0.20,Type.EAU);
        Monkepo chenipan = new Monkepo("Chenipan",false,false,"Chenille","Vert",0.10,Type.PLANTE);
        Monkepo noadkoko = new Monkepo("Noadkoko",false,true,"Palmier","Vert",0.70,Type.PLANTE);
//		Monkepo stud = new Monkepo("Stud", true, true);
//		Monkepo smogogo = new Monkepo("Smogogo", false, true);
//        Monkepo prof = new Monkepo("Prof", true, true);
//        Monkepo exam = new Monkepo("Exam",true, true);
//        Monkepo lucario = new Monkepo("Lucario",false,true);
//        Monkepo tarsal = new Monkepo("Tarsal",false,false);
//        Monkepo sabelette = new Monkepo("Sabelette",false,false);
                
        Attaque tranche = new Attaque("Tranche",15,3,Type.PLANTE);
        Attaque ecume = new Attaque("Ecume",10,2,Type.EAU);
        Attaque flammeche = new Attaque("Flammeche",10,2,Type.FEU);
        
        Attaque fouetL = new Attaque("Fouet Lianes",20,4,Type.PLANTE);
        Attaque pistoletAO = new Attaque("PistoletAO",15,3,Type.EAU);
        Attaque lanceF = new Attaque("Lance-flammes",25,6,Type.FEU);
        
        Attaque trancheH = new Attaque("Tranch'herbe",35,7,Type.PLANTE);
        Attaque surf = new Attaque("Surf",30,7,Type.EAU);
        Attaque rouefeu = new Attaque("Roue de feu",30,7,Type.FEU);
        
        Attaque lancesoleil = new Attaque("Lance-soleil",50,9,Type.PLANTE);
        Attaque hydrocanon = new Attaque("Hydrocanon",60,9,Type.EAU);
        Attaque surchauffe = new Attaque("Surchauffe",60,10,Type.FEU);
        
        ArrayList<Attaque> attaques = new ArrayList<>();
        
        attaques.add(tranche);
        attaques.add(ecume);
        attaques.add(flammeche);
        attaques.add(fouetL);
        
        pikachu.setListeAttaque(attaques);
        dracofeu.setListeAttaque(attaques);
        salameche.setListeAttaque(attaques);
        bulbizare.setListeAttaque(attaques);
        stud.setListeAttaque(attaques);
        magicarpe.setListeAttaque(attaques);
        chetiflor.setListeAttaque(attaques);
        caninos.setListeAttaque(attaques);
		
		joueur.ajouterABanque(pikachu);
		joueur.ajouterABanque(dracofeu);
		joueur.ajouterABanque(salameche);
		joueur.ajouterABanque(bulbizare);

		
		pnj1.placerDansLeSac(stud);
		pnj1.placerDansLeSac(magicarpe);
		pnj1.placerDansLeSac(chetiflor);
		pnj1.placerDansLeSac(caninos);
		pnj1.placerDansLeSac(feunard);
		
		
		joueur.afficherSac(); // fonctionne
		joueur.afficherBanque(); //fonctionne
		joueur.envoyerMonkepoBanque(pikachu);
		joueur.envoyerMonkepoBanque(dracofeu);
		joueur.envoyerMonkepoBanque(salameche);
		
		joueur.afficherSac();
		
		//HautesHerbes hh = new HautesHerbes(1, 1, false, false);
		
	 //	hh.placerMonkepoSurCase(dracofeu);
	//	hh.initialiserCombat(joueur);
		
		System.out.println("");
		Route r = new Route(2, 4, false, true);
		r.placerDresseurSurCase(pnj1);
		r.initialiserCombat(joueur);
		
	}

}
