
import java.util.ArrayList;

/**
 *
 * @author Utilisateur
 */
public abstract class Dresseur {
    private int badgeId;
    private static int nbBadge = 0 ;
    
    private Monkepo[] sacADos;
    private String nom , prenom;
    private double sommeMonkedollar;
    private ArrayList<Monkepo> banque;
    
    
    public Dresseur(String _nom, String _prenom, double _sommeMonkedollar){
        this.sacADos = new Monkepo[6];
        this.badgeId= this.nbBadge;
        this.nom= _nom;
        this.prenom= _prenom;
        this.sommeMonkedollar=_sommeMonkedollar;
        this.nbBadge++;
        
        this.banque = new ArrayList<>();
    }

    public int getBadgeId() {
        return badgeId;
    }

    public Monkepo[] getSacADos() {
        return sacADos;
    }

    public void setSacADos(Monkepo[] sacADos) {
        this.sacADos = sacADos;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public double getSommeMonkedollar() {
        return sommeMonkedollar;
    }
    
    public void setSommeMonkedollar(double somme) {
    	this.sommeMonkedollar = somme;
    }
    
    //methode présentation
    public void sePresenter (){
        System.out.println("Salut, je suis " + this.getNom() + " " + this.getPrenom() +", je vais te goumer !!!");
    }
        
    //methode Verser Somme
        
    public void verserSomme(Dresseur d){
            d.crediter(this.sommeMonkedollar*0.1);
            this.sommeMonkedollar *=0.9;
    }
        
    public void crediter (double gain){
            this.sommeMonkedollar += gain;
    }
        
    public boolean sacIsFull(){
         boolean isFull = true;
         
         for(Monkepo m : this.sacADos){
             if(m == null){
                 isFull = false;
             }
         }
         
         return isFull;  
    }
        
    public void placerDansLeSac (Monkepo m){
    	 int rang = 0; 
         for(int i = 0; i < this.sacADos.length; i++) {
        	 if(this.sacADos[i] == null) {
        		 rang = i;
        		 break;
        	 }
         }
         
         this.sacADos[rang] = m;
    }

    public ArrayList<Monkepo> getBanque() {
        return banque;
    }
    
    public void ajouterABanque ( Monkepo m){
        this.banque.add(m);
    }
    
    public void retirerBanque ( Monkepo m){
    	int rang = 0;
    	
    	for(int i = 0; i < this.banque.size(); i++) {
    		if(this.banque.get(i) == m) {
    			rang = i;
    			break;
    		}
    	}
    	
    	this.banque.remove(rang);
    }
    
     public void retirerSacADos ( Monkepo m){
     	int rang = 0;
    	
     	for(int i = 0; i < this.sacADos.length; i++) {
     		if(this.sacADos[i] == m) {
     			rang = i;
     			break;
     		}
     	}
     	
     	this.sacADos[rang] = null;
    }
     
    public void afficherSac() {
    	String result = "Sac a dos = [";
    	
    	for(Monkepo m : this.sacADos) {
    		if(m != null) {
    			result += m.getSurnom() + ", ";
    		} else {
    			result += m + ", ";
    		}	
    	}
    	
    	result += "]";
    	
    	System.out.println(result);
    }
    
    public void afficherBanque() {
    	String result = "Banque = [";
    	
    	for(Monkepo m : this.banque) {
    		result += m.getSurnom() + ", ";
    	}
    	
    	result += "]";
    	
    	System.out.println(result);
    }
    
    public boolean possedeEncoreMonkepo() {
    	boolean result = false;
    	
    	for(Monkepo m : this.sacADos) {
    		if(m != null) {
    			if(m.getPointVie() == 0) {
    				result = false;
    			} else {
    				result = true;
    				break;
    			}
    		}
    	}   
    	
    	return result;
    }
    
}
