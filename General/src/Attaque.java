/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realx
 */
public class Attaque {
    
    private String nom;
    private int valeurPuissance;
    private int coutPP;
    private Type typeAttaque;
    
    public Attaque(String _nom, int _valeurP, int _coutPP, Type _typeA) {
        this.nom = _nom;
        this.valeurPuissance = _valeurP;
        this.coutPP = _coutPP;
        this.typeAttaque = _typeA;
        
    }

    public String getNom() {
    	return this.nom;
    }
    
    public int getValeurPuissance() {
        return valeurPuissance;
    }

    public int getCoutPP() {
        return coutPP;
    }

    public Type getTypeAttaque() {
        return typeAttaque;
    }
    
   
    
    public boolean estLancable(Monkepo m) {
        
        if(m.getPointPouvoir()>this.coutPP) {
            
            return true;
        } else {
            return false;
        }
        
    }
    
    public int calculDegats() {
        
        return valeurPuissance;
        
    }
    
}
