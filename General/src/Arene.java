import java.time.LocalDate;
import java.util.ArrayList;

public class Arene extends Case{
	
	private Type typeArene;
	private ArrayList<NonJoueur> combattantsArene;

	public Arene(int _x, int _y, boolean _bloque, boolean _rapide, Type _typearene) {
		super(_x, _y, _bloque, _rapide);
		this.typeArene = _typearene;
		this.combattantsArene = new ArrayList<>();
	}
	
	public void placerLesCombattants(ArrayList<NonJoueur> _nonjoueur) {
		this.combattantsArene = _nonjoueur;
	}
	
	// Ecrire une methode sur 8 combats
	public void combatsEnArenes(Joueur j) {
		System.out.println("Combat numéro 1, bonne chance !");
		
		// Premier Combat, une initialisation
		Combat unCombat = new Combat(j, this.combattantsArene.get(0), null, LocalDate.now());
		boolean resultat = unCombat.combattreDresseur();
		int nbCombat = 1;
		
		// Tant que nos 7 combats ne sont pas fait et qu'on gagne
		while(nbCombat <= 7 && resultat) {
			System.out.println("");
			System.out.println("Combat numéro " + (nbCombat + 1) + ". Bonne chance !");
			
			unCombat = new Combat(j, this.combattantsArene.get(nbCombat), null, LocalDate.now());
			resultat = unCombat.combattreDresseur();
			nbCombat++;
		}
		
		// Si le dresseur gagne, on lui attribue le badge de champion d'Arène :
		if(resultat) {
			j.ajouterUnBadge("Vainqueur de l'Arène " + this.getTypeArene());
		} else {
			System.out.println("Tu as perdu le tournoi de l'Arène. Reviens quand tu seras de taille.");
		}
		
	}
	
	public Type getTypeArene() {
		return this.typeArene;
	}
}
