import java.util.ArrayList;

public class Kanto {
	private ArrayList<Dresseur> listeNonJoueurs;
	private ArrayList<Monkepo> listeMonkepos;
	private Carte carteDuMonde;
	private Joueur joueur;
	
	public Kanto() throws Exception{
		this.listeNonJoueurs = new ArrayList<>();
		this.listeMonkepos = new ArrayList<>();
		this.joueur = new Joueur("Test", "Test", 23);
		
		this.carteDuMonde = new Carte(this.joueur);
	}
	
	public void creerCarteKanto() throws Exception {
		// On Initialise la position du joueur
		int[][] temp = this.carteDuMonde.getTableauInt();
		temp[this.joueur.getPositionX()][this.joueur.getPositionY()] = 666;
		
		this.carteDuMonde.setTabloenint(temp);
		
		this.carteDuMonde.creerInstanceCarte();
	}
	
	public int[][] getMapInfos(){
		return this.carteDuMonde.getTableauInt();
	}
	
	public void setListeNonJoueurs(ArrayList<Dresseur> _liste) {
		this.listeNonJoueurs = _liste;
	}
	
	public void setListeMonkepos(ArrayList<Monkepo> _liste) {
		this.listeMonkepos = _liste;
	}

	public Carte getCarteDuMonde() {
		return carteDuMonde;
	}

	public void setCarteDuMonde(Carte carteDuMonde) {
		this.carteDuMonde = carteDuMonde;
	}

	public ArrayList<Dresseur> getListeNonJoueurs() {
		return listeNonJoueurs;
	}

	public ArrayList<Monkepo> getListeMonkepos() {
		return listeMonkepos;
	}
	
	public void ajouterDresseur(Dresseur _nonjoueur) {
		this.listeNonJoueurs.add(_nonjoueur);
	}
	
	public void ajouterMonkepo(Monkepo _monkepo) {
		this.listeMonkepos.add(_monkepo);
	}


}
