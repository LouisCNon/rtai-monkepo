import java.util.Random;

public class Infirmerie extends Case {

	public Infirmerie(int _x, int _y, boolean _bloque, boolean _rapide) {
		super(_x, _y, _bloque, _rapide);
	}

	@Override
	public void setBloque(boolean _obstacle) {
		this.bloque = false;
	}	
	
	
	// On suppose qu'il peut y avoir au max 3 dresseurs avant le joueur
	// on genere un nombre random de dresseur 
	// et on attend
	public void soignerMonkepo(Dresseur d) throws InterruptedException {
		Random randomDresseur = new Random();
		
		int nbDresseur = randomDresseur.nextInt(4);
		
		System.out.println("Traitement des " + nbDresseur + " Monkepos avant vous.");
		Thread.sleep(12000 * nbDresseur);
		
		
		System.out.println("Je m'occupe de vos Monkepos, attendez deux minutes.");
		
		for(Monkepo m : d.getSacADos()) {
			if(m != null) {
				m.setPointVie(100);
			}
		}
		
		// attendre deux minutes
		Thread.sleep(120000);
		
		System.out.println("Vos Monkepos sont soignes, passez une bonne journee");

	}
}
