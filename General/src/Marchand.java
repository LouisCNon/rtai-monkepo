import java.util.Scanner;

public class Marchand extends Case {

	public Marchand(int _x, int _y, boolean _bloque, boolean _rapide) {
		super(_x, _y, _bloque, _rapide);
	}
	
	public void acheterMonkeball(Joueur d) {
		System.out.println("Bonjour, combien de Monkeball souhaitez-vous ?");
		Scanner quantite = new Scanner(System.in);
		
		int quantiteInt = quantite.nextInt();
		
		if(quantiteInt * 10 <= d.getSommeMonkedollar()) {
			d.ajouterMonkeball(quantiteInt);
			d.setSommeMonkedollar(d.getSommeMonkedollar() - (quantiteInt * 10));
			System.out.println("Et voila les Monkeballs demandees, a bientot !!!");
		} else {
			System.out.println("Tu n'as pas l'argent requis pour cette quantite, la boutique ferme de toute facon");
		}
	}

}
