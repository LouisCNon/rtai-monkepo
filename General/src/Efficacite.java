/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realx
 */
public class Efficacite {
	
        private Monkepo m2;
        private Attaque a;
        
    public Efficacite(Monkepo _m2, Attaque _a) {
    	this.m2 = _m2;
    	this.a = _a;
    }
        
    public EfficaciteEnum CompareType() {
        
        if (this.a.getTypeAttaque() == Type.FEU && this.m2.getType() == Type.EAU){
            return EfficaciteEnum.PEU_EFFICACE;
        }
        else if (this.a.getTypeAttaque() == Type.FEU && this.m2.getType() == Type.PLANTE){
            return EfficaciteEnum.TRES_EFFICACE;
        }
        else if (this.a.getTypeAttaque() == Type.FEU && this.m2.getType() == Type.FEU) {
            return EfficaciteEnum.NORMAL;
        }
        
        if (this.a.getTypeAttaque() == Type.EAU && this.m2.getType() == Type.PLANTE){
            return EfficaciteEnum.PEU_EFFICACE;
        }
        else if (this.a.getTypeAttaque() == Type.EAU && this.m2.getType() == Type.FEU){
            return EfficaciteEnum.TRES_EFFICACE;
        }
        else if (this.a.getTypeAttaque() == Type.EAU && this.m2.getType() == Type.EAU) {
            return EfficaciteEnum.NORMAL;
        }       
        
        if (this.a.getTypeAttaque() == Type.PLANTE && this.m2.getType() == Type.FEU){
            return EfficaciteEnum.PEU_EFFICACE;
        }
        else if (this.a.getTypeAttaque() == Type.PLANTE && this.m2.getType() == Type.EAU){
            return EfficaciteEnum.TRES_EFFICACE;
        }
        
        return EfficaciteEnum.NORMAL;
    }
    
    public int calculerEfficacite() {
        
        int degats = this.a.getValeurPuissance();
        
        if (this.CompareType() == EfficaciteEnum.TRES_EFFICACE){
            return degats *= 2;
        }
        else if (this.CompareType() == EfficaciteEnum.NORMAL){
            return degats;
        }
        else if(this.CompareType() == EfficaciteEnum.PEU_EFFICACE) {
            return degats /= 2;
        }  
        
        return degats;
        
    }
            
    
}
