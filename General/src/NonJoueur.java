
public class NonJoueur extends Dresseur{

	private boolean estMilitant;
	private int positionX;
	private int positionY;
	
	public NonJoueur(String _nom, String _prenom, double _sommeMonkedollar, boolean _estMilitant, int _positionX, int _positionY) {
		super(_nom, _prenom, _sommeMonkedollar);
		
		this.estMilitant = _estMilitant;
		this.positionX = _positionX;
		this.positionY = _positionY;
	}

	
	public boolean getEstMilitant() {
		return this.estMilitant;
	}
	
	public void setEstMilitant(boolean _estMilitant) {
		this.estMilitant = _estMilitant;
	}
	
	public void volerMonkepo(Dresseur d) {
		if(this.estMilitant) {
			// vole le Monkepo de rang le plus haut dans le sac
			Monkepo[] nouveauSacDresseur = d.getSacADos();
			
			nouveauSacDresseur[0] = null;
			
			d.setSacADos(nouveauSacDresseur);				
		} else {
			System.out.println("Je ne volerai pas ton Monkepo, je ne suis pas un Militant");
		}
 	}
}
