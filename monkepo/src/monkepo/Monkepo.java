/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monkepo;

/**
 *
 * @author Sylvain le bg
 */
public class Monkepo {

    private int IDunique;
    private String surnom;
    private int niveau;
    private int pointVie;
    private int pointExperience;
    private int pointPouvoir;
    private boolean estEvolution;
    private boolean estUnique;

    public Monkepo(Integer IDunique, String surnom, Integer niveau, Integer pointVie, Integer pointExperience,
            Integer pointPouvoir, Boolean estEvolution, Boolean estUnique) {
        this.IDunique = IDunique;
        this.surnom = surnom;
        this.niveau = niveau;
        this.pointVie = pointVie;
        this.pointExperience = pointExperience;
        this.pointPouvoir = pointPouvoir;
        this.estEvolution = estEvolution;
        this.estUnique = estUnique;
    };

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

    }

    /**
     * @return int return the IDunique
     */
    public int getIDunique() {
        return IDunique;
    }

    /**
     * @return String return the surnom
     */
    public String getSurnom() {
        return surnom;
    }

    /**
     * @return int return the niveau
     */
    public int getNiveau() {
        return niveau;
    }

    /**
     * @param niveau the niveau to set
     */
    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    /**
     * @return int return the pointVie
     */
    public int getPointVie() {
        return pointVie;
    }

    /**
     * @param pointVie the pointVie to set
     */
    public void setPointVie(int pointVie) {
        this.pointVie = pointVie;
    }

    /**
     * @return int return the pointExperience
     */
    public int getPointExperience() {
        return pointExperience;
    }

    /**
     * @param pointExperience the pointExperience to set
     */
    public void setPointExperience(int pointExperience) {
        this.pointExperience = pointExperience;
    }

    /**
     * @return int return the pointPouvoir
     */
    public int getPointPouvoir() {
        return pointPouvoir;
    }

    /**
     * @param pointPouvoir the pointPouvoir to set
     */
    public void setPointPouvoir(int pointPouvoir) {
        this.pointPouvoir = pointPouvoir;
    }

    public Boolean estKo() {
        if (pointVie == 0) {
            return true;
        } else
            return false;
    }

    public void calculerNiveau(Monkepo b) {
        while (b.getPointExperience() >= 100 && b.getNiveau() < 100) {
            b.setNiveau(b.getNiveau() + 1);
            b.setPointExperience(b.getPointExperience() - 100);

        }

    }

    public Boolean peutEvoluer(Monkepo x) {
        boolean res = false;
        if (x.getNiveau() == 100) {
            if (estEvolution == true) {
                res = false;
            } else
                res = true;
        }
        return res;
    }

    public void estUnique(Monkepo z) {
        if (z.estUnique == true) {
            z.setNiveau(100);
        }
    }

    public Boolean estDansSac() {
        return estDansSac();
    }

}
