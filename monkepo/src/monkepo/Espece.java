/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monkepo;

/**
 *
 * @author ulyss
 */
public abstract class Espece {
    
    private String nom;
    private String couleur;
    private double taille;
    
    public Espece(String nom, String couleur, double taille) {
        this.nom = nom;
        this.couleur = couleur;
        this.taille = taille;
    }
    
    public String getNom() {
        return nom;
    }

    public String getCouleur() {
        return couleur;
    }

    public double getTaille() {
        return taille;
    }
    
    
    public void evoluer() {
        // TODO code application logic here
    }

    
}
