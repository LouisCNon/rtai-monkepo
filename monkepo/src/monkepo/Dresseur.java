/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monkepo;

import java.util.ArrayList;

/**
 *
 * @author Utilisateur
 */
public abstract class Dresseur {
    private int badgeId;
    private static int nbBadge = 0 ;
    private Monkepo[] sacADos;
    private String nom , prenom;
    private double sommeMonkedollar;
    private ArrayList<Monkepo> banque;
    
    
    public Dresseur(String _nom, String _prenom, double _sommeMonkedollar){
        this.sacADos = new Monkepo[6];
        this.badgeId= this.nbBadge;
        this.nom= _nom;
        this.prenom= _prenom;
        this.sommeMonkedollar=_sommeMonkedollar;
        this.nbBadge++;
        
        this.banque = new ArrayList<>();
    }

    public int getBadgeId() {
        return badgeId;
    }

    public Monkepo[] getSacADos() {
        return sacADos;
    }

    public void setSacADos(Monkepo[] sacADos) {
        this.sacADos = sacADos;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public double getSommeMonkedollar() {
        return sommeMonkedollar;
    }
    
    //methode présentation
    public void sePresenter (){
        System.out.println("Salut, je suis " + this.getNom() + " " + this.getPrenom() +", je vais te goumer !!!");
    }
        
    //methode Verser Somme
        
    public void verserSomme(Dresseur d){
            d.crediter(this.sommeMonkedollar*0.1);
            this.sommeMonkedollar *=0.9;
        }
        
    public void crediter (double gain){
            this.sommeMonkedollar += gain;
        }
        
    public boolean sacIsFull(){
            boolean isFull= false;
            for(Monkepo m : this.sacADos){
                if(m==null){
                    isFull=true;
                }
            }
         return isFull;  
        }
        
    public void placerDansLeSac (int rang, Monkepo m){
            if (rang<0 || rang>5){
                System.out.println("Depassement de la taille du tableau");
            } else {
                this.sacADos[rang] = m;
            }
        }

    public ArrayList<Monkepo> getBanque() {
        return banque;
    }
    
    public void ajouterABanque ( Monkepo m){
        this.banque.add(m);
    }
    
    public void retirerBanque ( Monkepo m){
        for( Monkepo dansBanque : this.banque){
            if(dansBanque == m){
                this.banque.remove(dansBanque);
            }
        }
    }
    
     public void retirerSacADos ( Monkepo m){
        for( Monkepo dansBanque : this.banque){
            if(dansBanque == m){
                this.banque.remove(dansBanque);
            }
        }
    }
}
